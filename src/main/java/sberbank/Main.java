package sberbank;


import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import sberbank.domain.Order;
import sberbank.domain.Parameter;
import sberbank.domain.Service;
import sberbank.service.CSVService;
import sberbank.service.DBService;
import sberbank.service.XMLService;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.File;
import java.io.StringReader;
import java.io.StringWriter;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

public class Main {

    private static final String DOCUMENT_TYPE = "ВИД_ДОК";
    private static final String CITIZENSHIP = "ГРАЖДАНСТВО";
    private static DBService dbService;

    public static void main(String[] args) throws SQLException {

        try {
            XMLService<Order> orderXMLService = new XMLService<>(Order.class);
            CSVService csvService = new CSVService();
            dbService = new DBService("jdbc:h2:~/test", "sa", "");
            dbService.executeUpdate("DROP ALL OBJECTS");

            Order order = orderXMLService.toObject(XML.VALUE);

            // #1. Необходимо сформировать коллекцию, содержащую все виды документов в отсортированном порядке.
            List<String> documentTypes = order.getServices()
                    .stream()
                    .flatMap(s -> s.getParameters().stream())
                    .filter(p -> p.getName().equals(DOCUMENT_TYPE))
                    .flatMap(p -> p.getParameterValues().stream())
                    .map(Parameter.ParameterValue::getValue)
                    .sorted()
                    .collect(Collectors.toList());

            // #2. Вывести имена и значения всех атрибутов для par step="1" name="ГРАЖДАНСТВО"
            List<Parameter> citizenshipList = order.getServices()
                    .stream()
                    .flatMap(s -> s.getParameters().stream())
                    .filter(p -> p.getStep() == 1)
                    .filter(p -> p.getName().equals(CITIZENSHIP))
                    .collect(Collectors.toList());

            for (Parameter citizenship : citizenshipList) {
                citizenship.setParameterValues(null);

                // Вывод в виде XML в консоль
                String XML = orderXMLService.toXML(citizenship, Order.class);
                System.out.println(XML);

                // Вывод в виде CSV в файл
                csvService.toCSV(XML);
            }
            // #3.  Задача со звездочкой: создать в базе таблицу-справочник со значениями из первой части

            dbService.executeUpdate("CREATE TABLE IF NOT EXISTS SBERBANK_DOCUMENT_TYPE ( \n" +
                    "   ID BIGINT AUTO_INCREMENT NOT NULL, \n" +
                    "   S_TYPE VARCHAR(50) NOT NULL, \n" +
                    ");");

            for (String documentType : documentTypes) {
                dbService.addBatch("INSERT INTO SBERBANK_DOCUMENT_TYPE (S_TYPE) VALUES ('" + documentType + "');");
            }
            dbService.executeBatch();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            dbService.closeConnection();
        }
    }
}
