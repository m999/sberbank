package sberbank.domain;

import javax.xml.bind.annotation.*;
import java.util.List;

@XmlRootElement(name = "par")
@XmlAccessorType(XmlAccessType.FIELD)
public class Parameter {

    @XmlAttribute(name = "step")
    private int step;

    @XmlAttribute(name = "name")
    private String name;

    @XmlAttribute(name = "fullname")
    private String fullName;

    @XmlAttribute(name = "comment")
    private String comment;

    @XmlAttribute(name = "isEditable")
    private boolean editable;

    @XmlAttribute(name = "isScanable")
    private boolean scanable;

    @XmlAttribute(name = "isVisible")
    private boolean visible;

    @XmlAttribute(name = "isRequired")
    private boolean required;

    @XmlAttribute(name = "isPrinted")
    private boolean printed;

    @XmlAttribute(name = "isValidateOnLine")
    private boolean validateOnLine;

    @XmlAttribute(name = "type")
    private String type;

    @XmlAttribute(name = "min_length")
    private int minLength;

    @XmlAttribute(name = "max_length")
    private int maxLength;

    @XmlAttribute(name = "double_input")
    private boolean doubleInput;

    @XmlAttribute(name = "value")
    private String value;

    @XmlAttribute(name = "reg_exp")
    private String regExp;

    @XmlAttribute(name = "from_debt")
    private boolean fromDebt;

    @XmlElement(name = "par_list")
    private List<ParameterValue> parameterValues;

    public int getStep() {
        return step;
    }

    public void setStep(int step) {
        this.step = step;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }

    public boolean isScanable() {
        return scanable;
    }

    public void setScanable(boolean scanable) {
        this.scanable = scanable;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public boolean isPrinted() {
        return printed;
    }

    public void setPrinted(boolean printed) {
        this.printed = printed;
    }

    public boolean isValidateOnLine() {
        return validateOnLine;
    }

    public void setValidateOnLine(boolean validateOnLine) {
        this.validateOnLine = validateOnLine;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getMinLength() {
        return minLength;
    }

    public void setMinLength(int minLength) {
        this.minLength = minLength;
    }

    public int getMaxLength() {
        return maxLength;
    }

    public void setMaxLength(int maxLength) {
        this.maxLength = maxLength;
    }

    public boolean isDoubleInput() {
        return doubleInput;
    }

    public void setDoubleInput(boolean doubleInput) {
        this.doubleInput = doubleInput;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getRegExp() {
        return regExp;
    }

    public void setRegExp(String regExp) {
        this.regExp = regExp;
    }

    public boolean isFromDebt() {
        return fromDebt;
    }

    public void setFromDebt(boolean fromDebt) {
        this.fromDebt = fromDebt;
    }

    public List<ParameterValue> getParameterValues() {
        return parameterValues;
    }

    public void setParameterValues(List<ParameterValue> parameterValues) {
        this.parameterValues = parameterValues;
    }

    @XmlRootElement(name = "par_list")
    @XmlAccessorType(XmlAccessType.FIELD)
    public static class ParameterValue {

        @XmlAttribute(name = "value")
        private String value;

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }
}
