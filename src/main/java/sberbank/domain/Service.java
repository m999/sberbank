package sberbank.domain;

import javax.xml.bind.annotation.*;
import java.util.List;

@XmlRootElement(name = "serv")
@XmlAccessorType(XmlAccessType.FIELD)
public class Service {

    @XmlAttribute(name = "isClosed")
    private boolean closed;

    @XmlElement(name = "serv_id")
    private String id;

    @XmlElement(name = "bic")
    private int bic;

    @XmlElement(name = "schet")
    private int schet;

    @XmlElement(name = "corr_schet")
    private int corrSchet;

    @XmlElementWrapper(name = "pars")
    @XmlElement(name = "par")
    private List<Parameter> parameters;

    @XmlElement(name = "sys_message")
    private String sysMessage;

    public boolean isClosed() {
        return closed;
    }

    public void setClosed(boolean closed) {
        this.closed = closed;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getBic() {
        return bic;
    }

    public void setBic(int bic) {
        this.bic = bic;
    }

    public int getSchet() {
        return schet;
    }

    public void setSchet(int schet) {
        this.schet = schet;
    }

    public int getCorrSchet() {
        return corrSchet;
    }

    public void setCorrSchet(int corrSchet) {
        this.corrSchet = corrSchet;
    }

    public List<Parameter> getParameters() {
        return parameters;
    }

    public void setParameters(List<Parameter> parameters) {
        this.parameters = parameters;
    }

    public String getSysMessage() {
        return sysMessage;
    }

    public void setSysMessage(String sysMessage) {
        this.sysMessage = sysMessage;
    }
}
