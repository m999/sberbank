package sberbank.service;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DBService {

    private Connection connection;
    private List<String> batch = new ArrayList<>();

    public DBService(String url, String username, String password) throws SQLException {
        this.connection = DriverManager.getConnection(url, username, password);
    }

    public void executeUpdate(String SQL) throws SQLException {
        Statement statement = connection.createStatement();
        statement.executeUpdate(SQL);
    }

    public void addBatch(String SQL) {
        batch.add(SQL);
    }

    public void executeBatch() throws SQLException {
        Statement statement = connection.createStatement();
        for (String s : batch) {
            statement.addBatch(s);
        }
        statement.executeBatch();
    }

    public ResultSet executeQuery(String SQL) throws SQLException {
        Statement statement = connection.createStatement();
        return statement.executeQuery(SQL);
    }

    public void closeConnection() throws SQLException {
        this.connection.close();
    }
}
