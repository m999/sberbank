package sberbank.service;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;

public class CSVService {

    public void toCSV(String XML) throws ParserConfigurationException, IOException, SAXException, TransformerException {
        Document document = DocumentBuilderFactory.newInstance()
                .newDocumentBuilder()
                .parse(new InputSource(new StringReader(XML)));

        StreamSource styleSource = new StreamSource(new File("src/main/resources/style.xsl"));
        Source source = new DOMSource(document);
        Result outputTarget = new StreamResult(new File("tmp/citizenship.csv"));

        TransformerFactory.newInstance()
                .newTransformer(styleSource)
                .transform(source, outputTarget);
    }
}
