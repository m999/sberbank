package sberbank.service;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;
import java.io.StringWriter;

public class XMLService<T> {

    private JAXBContext jaxbContext;
    private Class<T> objectClass;

    public XMLService(Class<T> objectClass) throws JAXBException {
        this.jaxbContext = JAXBContext.newInstance(objectClass);
        this.objectClass = objectClass;
    }

    public T toObject(String xml) throws JAXBException {
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        return objectClass.cast(unmarshaller.unmarshal(new StringReader(xml)));
    }

    public String toXML(Object object, Class<T> objectClass) throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(objectClass);
        Marshaller marshaller = jaxbContext.createMarshaller();
        StringWriter stringWriter = new StringWriter();
        marshaller.marshal(object, stringWriter);
        return stringWriter.toString();
    }
}
