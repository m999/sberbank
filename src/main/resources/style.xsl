<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="text"/>

    <xsl:template match="/">
        <!-- header -->
        <xsl:text>"attribute name","attribute value"&#10;</xsl:text>
        <!-- data rows -->
        <xsl:for-each select="par">
            <!-- data cells -->
            <xsl:for-each select="@*">
                <xsl:text>"</xsl:text>
                <xsl:value-of select="name()" />
                <xsl:text>"</xsl:text>
                <xsl:text>,</xsl:text>
                <xsl:text>"</xsl:text>
                <xsl:value-of select="." />
                <xsl:text>"</xsl:text>
                <xsl:text>&#xa;</xsl:text>
            </xsl:for-each>
        </xsl:for-each>
    </xsl:template>

</xsl:stylesheet>