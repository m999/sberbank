package sberbank;

import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;
import sberbank.domain.Order;
import sberbank.domain.Parameter;
import sberbank.service.CSVService;
import sberbank.service.DBService;
import sberbank.service.XMLService;

import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

import static com.sun.org.apache.xml.internal.dtm.ref.ExpandedNameTable.DOCUMENT_TYPE;
import static org.junit.Assert.*;

public class MainTest {

    private static final String DOCUMENT_TYPE = "ВИД_ДОК";
    private static final String CITIZENSHIP = "ГРАЖДАНСТВО";
    private DBService dbService;
    private XMLService<Order> orderXMLService;
    private CSVService csvService;

    @Before
    public void init() throws JAXBException, SQLException {
        orderXMLService = new XMLService<>(Order.class);
        csvService = new CSVService();
        dbService = new DBService("jdbc:h2:~/test", "sa", "");
        dbService.executeUpdate("DROP ALL OBJECTS");
    }

    @Test
    public void createSortedCollection() throws JAXBException {
        List<String> expected = Arrays.asList(
                "ВОДИТ УДОСТОВЕРЕНИЕ",
                "ИНН",
                "ОХОТНИЧИЙ БИЛЕТ",
                "ПАСПОРТ МОРЯКА",
                "ПАСПОРТ РФ",
                "РАЗРЕШ НА ОРУЖИЕ",
                "СВИД О РОЖДЕНИИ",
                "СВИД_РЕГ_ТС",
                "УДОСТОВЕР ВОЕНСЛУЖ");

        Order order = orderXMLService.toObject(XML.VALUE);
        List<String> documentTypes = order.getServices()
                .stream()
                .flatMap(s -> s.getParameters().stream())
                .filter(p -> p.getName().equals(DOCUMENT_TYPE))
                .flatMap(p -> p.getParameterValues().stream())
                .map(Parameter.ParameterValue::getValue)
                .sorted()
                .collect(Collectors.toList());
        Assert.assertThat(expected, CoreMatchers.is(documentTypes));
    }

    @Test
    public void getXMLAttributes() throws JAXBException {
        Order order = orderXMLService.toObject(XML.VALUE);

        String expectedXMLwithoutParameterValues = "" +
                "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" +
                "<par step=\"1\" " +
                "name=\"ГРАЖДАНСТВО\" " +
                "fullname=\"Гражданство\" " +
                "comment=\"Выберите гражданство, лица за кого оплачивается услуга, из списка\" " +
                "isEditable=\"true\" " +
                "isScanable=\"false\" " +
                "isVisible=\"true\" " +
                "isRequired=\"true\" " +
                "isPrinted=\"true\" " +
                "isValidateOnLine=\"true\" " +
                "type=\"M\" min_length=\"0\" " +
                "max_length=\"250\" " +
                "double_input=\"false\" " +
                "value=\"РОССИЯ\" reg_exp=\"\" " +
                "from_debt=\"false\"/>";

        List<Parameter> citizenshipList = order.getServices()
                .stream()
                .flatMap(s -> s.getParameters().stream())
                .filter(p -> p.getStep() == 1)
                .filter(p -> p.getName().equals(CITIZENSHIP))
                .collect(Collectors.toList());

        for (Parameter citizenship : citizenshipList) {
            citizenship.setParameterValues(null);
            String XML = orderXMLService.toXML(citizenship, Order.class);
            Assert.assertEquals(expectedXMLwithoutParameterValues, XML);
        }
    }

    @Test
    public void getCSVAttributes() throws IOException, JAXBException, ParserConfigurationException, TransformerException, SAXException {

        List<List<String>> expected = new ArrayList<>();
        expected.add(Arrays.asList("\"attribute name", "attribute value\""));
        expected.add(Arrays.asList("\"comment", "Выберите гражданство, лица за кого оплачивается услуга, из списка\""));
        expected.add(Arrays.asList("\"double_input", "false\""));
        expected.add(Arrays.asList("\"from_debt", "false\""));
        expected.add(Arrays.asList("\"fullname", "Гражданство\""));
        expected.add(Arrays.asList("\"isEditable", "true\""));
        expected.add(Arrays.asList("\"isPrinted", "true\""));
        expected.add(Arrays.asList("\"isRequired", "true\""));
        expected.add(Arrays.asList("\"isScanable", "false\""));
        expected.add(Arrays.asList("\"isValidateOnLine", "true\""));
        expected.add(Arrays.asList("\"isVisible", "true\""));
        expected.add(Arrays.asList("\"max_length", "250\""));
        expected.add(Arrays.asList("\"min_length", "0\""));
        expected.add(Arrays.asList("\"name", "ГРАЖДАНСТВО\""));
        expected.add(Arrays.asList("\"reg_exp", "\""));
        expected.add(Arrays.asList("\"step", "1\""));
        expected.add(Arrays.asList("\"type", "M\""));
        expected.add(Arrays.asList("\"value", "РОССИЯ\""));

        Order order = orderXMLService.toObject(XML.VALUE);
        List<Parameter> citizenshipList = order.getServices()
                .stream()
                .flatMap(s -> s.getParameters().stream())
                .filter(p -> p.getStep() == 1)
                .filter(p -> p.getName().equals(CITIZENSHIP))
                .collect(Collectors.toList());

        List<List<String>> records = new ArrayList<>();

        for (Parameter citizenship : citizenshipList) {
            citizenship.setParameterValues(null);
            String XML = orderXMLService.toXML(citizenship, Order.class);
            csvService.toCSV(XML);

            try (Scanner scanner = new Scanner(new File("tmp/citizenship.csv"))) {
                while (scanner.hasNextLine()) {
                    records.add(getRecordFromLine(scanner.nextLine()));
                }
            }
        }

        Assert.assertThat(expected, CoreMatchers.is(records));
    }

    @Test
    public void createTable() throws SQLException, JAXBException {

        List<String> expected = Arrays.asList(
                "ВОДИТ УДОСТОВЕРЕНИЕ",
                "ИНН",
                "ОХОТНИЧИЙ БИЛЕТ",
                "ПАСПОРТ МОРЯКА",
                "ПАСПОРТ РФ",
                "РАЗРЕШ НА ОРУЖИЕ",
                "СВИД О РОЖДЕНИИ",
                "СВИД_РЕГ_ТС",
                "УДОСТОВЕР ВОЕНСЛУЖ");

        Order order = orderXMLService.toObject(XML.VALUE);

        List<String> documentTypes = order.getServices()
                .stream()
                .flatMap(s -> s.getParameters().stream())
                .filter(p -> p.getName().equals(DOCUMENT_TYPE))
                .flatMap(p -> p.getParameterValues().stream())
                .map(Parameter.ParameterValue::getValue)
                .sorted()
                .collect(Collectors.toList());

        dbService.executeUpdate("CREATE TABLE IF NOT EXISTS SBERBANK_DOCUMENT_TYPE ( \n" +
                "   ID BIGINT AUTO_INCREMENT NOT NULL, \n" +
                "   S_TYPE VARCHAR(50) NOT NULL, \n" +
                ");");

        for (String documentType : documentTypes) {
            dbService.addBatch("INSERT INTO SBERBANK_DOCUMENT_TYPE (S_TYPE) VALUES ('" + documentType + "');");
        }
        dbService.executeBatch();
        ResultSet resultSet = dbService.executeQuery("SELECT S_TYPE FROM SBERBANK_DOCUMENT_TYPE");

        List<String> result = new ArrayList<>();
        while (resultSet.next()) {
            result.add(resultSet.getString(1));
        }

        Assert.assertThat(expected, CoreMatchers.is(result));
    }

    private List<String> getRecordFromLine(String line) {
        List<String> values = new ArrayList<>();
        try (Scanner rowScanner = new Scanner(line)) {
            rowScanner.useDelimiter("\"," + "\"");
            while (rowScanner.hasNext()) {
                values.add(rowScanner.next());
            }
        }
        return values;
    }
}